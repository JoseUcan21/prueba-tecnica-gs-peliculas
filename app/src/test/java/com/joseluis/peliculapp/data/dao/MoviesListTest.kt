package com.joseluis.peliculapp.data.dao

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class MoviesListTest{
    lateinit var mMovieList:MoviesList

    @Before
    fun setUp(){
        mMovieList = MoviesList()
    }

    @Test
    fun defaultListIsNotEmpty()  {
        assertEquals(true, mMovieList.getMoviesDefaultList2().size>0)
    }
}