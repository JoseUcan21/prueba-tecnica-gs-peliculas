package com.joseluis.peliculapp.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.joseluis.peliculapp.data.models.DetailMovies
import com.joseluis.peliculapp.providers.MoviesProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject



@HiltViewModel
class MainViewModel @Inject constructor(
    private val moviesProvider: MoviesProvider
) : ViewModel() {
    var listaPlayingNowMD   = MutableLiveData<ArrayList<DetailMovies>>()
    var listaPopularMD      = MutableLiveData<ArrayList<DetailMovies>>()
    var errorMsg            = MutableLiveData<String>()
    fun obtenerPlayingNowList(){
        try{
            viewModelScope.launch {
                val listaPlayingNow = withContext(Dispatchers.IO){
                    moviesProvider.getMoviesPlayingNow()
                }
                listaPlayingNowMD.value = listaPlayingNow
            }
        }catch (ex: Exception) {
            errorMsg.value = ex.message.toString()
            Log.e(this::class.simpleName.toString(), ex.message.toString())
        }
    }

    fun obtenerPopularList(){
        try{
            viewModelScope.launch {
                val listaPopular = withContext(Dispatchers.IO){
                    moviesProvider.getMoviesPopular()
                }
                listaPopularMD.value = listaPopular
            }
        }catch (ex: Exception) {
            errorMsg.value = ex.message.toString()
            Log.e(this::class.simpleName.toString(), ex.message.toString())
        }
    }

}