package com.joseluis.peliculapp.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.joseluis.peliculapp.providers.MoviesProvider
import com.joseluis.peliculapp.utils.Constantes
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class VideoViewModel @Inject constructor(
    private val moviesProvider: MoviesProvider
) : ViewModel() {
    var keyYt       = MutableLiveData<String>()
    var errorMsg    = MutableLiveData<String>()

    fun obtenerVideosByMovieId(id:String){
        try{
            viewModelScope.launch {
                val listaVideos = withContext(Dispatchers.IO){
                    moviesProvider.getVideoByMovie(id)
                }
                if(listaVideos.size > 0){
                    keyYt.value = "${Constantes.URL_YT}${listaVideos[0].key}"
                }
                else{
                    errorMsg.value = Constantes.ERROR_VIDEO
                }
            }
        }catch (ex:Exception){
            errorMsg.value = ex.message.toString()
            Log.e(this::class.simpleName.toString(), ex.message.toString())
        }
    }
}