package com.joseluis.peliculapp.data.models

data class VideoResponse(
    val id: Int,
    val results: ArrayList<DetailVideo>
)