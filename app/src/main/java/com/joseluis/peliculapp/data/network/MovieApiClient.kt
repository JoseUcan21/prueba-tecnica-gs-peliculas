package com.joseluis.peliculapp.data.network

import com.joseluis.peliculapp.data.models.MovieDbResponse
import com.joseluis.peliculapp.data.models.VideoResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApiClient {
    @GET("movie/now_playing")
    fun getLisyPlayingNow(@Query("api_key")api_key:String,
                          @Query("language")language:String,
                          @Query("page")page:Int ): Call<MovieDbResponse>

    @GET("movie/popular")
    fun getLisyPopular(@Query("api_key")api_key:String,
                          @Query("language")language:String,
                          @Query("page")page:Int ): Call<MovieDbResponse>

    ////  https://api.themoviedb.org/3/movie/580489/videos?api_key=4996541610d99f140ee83be6661dbb84&language=en-US
    @GET("movie/{movie}/videos")
    fun getVideoById(@Path("movie") movie:String,
                     @Query("api_key")api_key:String,
                     @Query("language")language:String):Call<VideoResponse>
}