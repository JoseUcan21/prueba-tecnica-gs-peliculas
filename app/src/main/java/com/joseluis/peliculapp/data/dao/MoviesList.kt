package com.joseluis.peliculapp.data.dao

import com.joseluis.peliculapp.R
import com.joseluis.peliculapp.data.models.DetailMovies

class MoviesList {
    private var listaMovies: ArrayList<DetailMovies> = arrayListOf<DetailMovies>(

        DetailMovies(
            id = 580489, original_title = "Venom: Let There Be Carnage",
            overview = "Eddie Brock (Tom Hardy) y su simbionte Venom todavía están intentando descubrir cómo vivir juntos cuando un preso que está en el corredor de la muerte (Woody Harrelson) se infecta con un simbionte propio.",
            vote_average = 7.2,portada_id = R.drawable.back_venom,poster_id = R.drawable.poster_venom),
        DetailMovies(
            id = 512195, original_title = "Red Notice",
            overview = "Cuando la Interpol envía una \"Alerta roja\", significa que los departamentos de Policía de todo el mundo deben estar alerta para capturar a los criminales más buscados. Todas las alarmas saltan cuando un temerario robo une al mejor agente del FBI (Johnson) con dos criminales rivales entre sí (Gadot & Reynolds). Una coincidencia que hará que suceda lo impredecible.",
            vote_average = 6.8,portada_id = R.drawable.back_red_notice,poster_id = R.drawable.poster_red_notice
        ),
        DetailMovies(
            id = 568124, original_title = "Encanto",
            overview = "\"Encanto\" relata la historia de los Madrigal, una familia extraordinaria que vive en una casa mágica de un pueblo vibrante en las montañas de Colombia escondidas en un “Encanto”. La magia del Encanto ha bendecido a cada niño de la familia con un don único, desde la superfuerza hasta el poder de sanar. A todos, excepto Mirabel, quien desea ser tan especial como el resto de su familia.  Pero cuando la magia que rodea al Encanto está en peligro, Mirabel decide que ella, la única Madrigal sin ningún tipo de don único, puede ser la única esperanza de su excepcional familia.",
            vote_average = 7.4,portada_id = R.drawable.back_encanto,poster_id = R.drawable.poster_encanto
        ),
        DetailMovies(
            id = 585245, original_title = "Clifford the Big Red Dog",
            overview = "Cuando Emily Elizabeth conoce a un rescatador mágico de animales que le regala un pequeño cachorro rojo, nunca se hubiera imaginado que al despertarse se encontraría un sabueso gigante de tres metros en su pequeño apartamento de Nueva York. Mientras su madre soltera se encuentra de viaje de negocios, Emily y su divertido pero impulsivo tío Casey se embarcan en una gran aventura.",
            vote_average = 7.6,portada_id = R.drawable.back_clifford,poster_id = R.drawable.poster_clifford
        ),
        DetailMovies(
            id = 617653, original_title = "The Last Duel",
            overview = "Ambientada en Francia, en 1386, cuenta el enfrentamiento entre el caballero Jean de Carrouges y el escudero Jacques LeGris , al acusar el primero al segundo de abusar de su esposa, Marguerite de Carrouges. El Rey Carlos VI decide que la mejor forma de solucionar el conflicto es un duelo a muerte. El que venza será el ganador, sin embargo, si lo hace el escudero, la esposa del caballero será quemada como castigo por falsas acusaciones.",
            vote_average = 7.5,portada_id = R.drawable.back_last_duel,poster_id = R.drawable.poster_last_duel
        )

    )

    suspend fun getMoviesDefaultList() :ArrayList<DetailMovies>{
        return listaMovies
    }

    fun getMoviesDefaultList2() :ArrayList<DetailMovies>{
        return listaMovies
    }

    /*
    *
    *
    * */
}