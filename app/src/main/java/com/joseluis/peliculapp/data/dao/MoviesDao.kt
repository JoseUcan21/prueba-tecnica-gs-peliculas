package com.joseluis.peliculapp.data.dao

import com.joseluis.peliculapp.data.models.DetailMovies
import javax.inject.Inject

//simulamos una base de datos en caso de que no haya wifi
class MoviesDao @Inject constructor() {
    lateinit var mMoviesList: MoviesList
    init{
        mMoviesList = MoviesList()
    }
    suspend fun recuperaNowPlayingDb():ArrayList<DetailMovies>{
        return mMoviesList.getMoviesDefaultList()
    }

    suspend fun recuperaPopularDb():ArrayList<DetailMovies>{
        return mMoviesList.getMoviesDefaultList()
    }
}