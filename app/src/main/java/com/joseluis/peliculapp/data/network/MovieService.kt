package com.joseluis.peliculapp.data.network

import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.joseluis.peliculapp.adaptaders.PeliculasAdapter
import com.joseluis.peliculapp.config.AplicationPeliculaApp
import com.joseluis.peliculapp.data.models.DetailMovies
import com.joseluis.peliculapp.data.models.DetailVideo
import com.joseluis.peliculapp.data.models.MovieDbResponse
import com.joseluis.peliculapp.utils.Constantes
import retrofit2.Call
import retrofit2.Response
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class MovieService @Inject constructor(
    private val apiClient: MovieApiClient
) {

    suspend fun recuperaNowPlayingApi():ArrayList<DetailMovies>{
        //var resp = AplicationPeliculaApp.webServiceGlobal.getLisyPlayingNow(
        //            Constantes.API_KEY, Constantes.LANGUAJE, Constantes.PAGE)
        var resp = apiClient.getLisyPlayingNow(Constantes.API_KEY, Constantes.LANGUAJE, Constantes.PAGE)

        try{
            var response = resp.execute()
            if(response.isSuccessful){
                Log.e("respuesta ","succesful")
                var body = response.body()
                if(body !=null){
                    Log.e("respuesta ","body correcto")
                    return body.results
                }else{
                    Log.e("respuesta ","body null")
                    return ArrayList<DetailMovies>()
                }
            }else{
                Log.e("respuesta ","no succesful")
                return ArrayList<DetailMovies>()
            }
        }catch(e:Exception){
            Log.e("respuesta catch",e.message.toString())
            return ArrayList<DetailMovies>()
        }
    }
    suspend fun recuperaPopularApi():ArrayList<DetailMovies>{
        var resp = apiClient.getLisyPopular(
            Constantes.API_KEY, Constantes.LANGUAJE, Constantes.PAGE)

        try{
            var response = resp.execute()
            if(response.isSuccessful){
                Log.e("respuesta ","succesful")
                var body = response.body()
                if(body !=null){
                    Log.e("respuesta ","body correcto")
                    return body.results
                }else{
                    Log.e("respuesta ","body null")
                    return ArrayList<DetailMovies>()
                }
            }else{
                Log.e("respuesta ","no succesful")
                return ArrayList<DetailMovies>()
            }
        }catch(e:Exception){
            Log.e("respuesta catch",e.message.toString())
            return ArrayList<DetailMovies>()
        }
    }
    suspend fun recuperaVideos(idMovie:String):ArrayList<DetailVideo>{
        var resp = apiClient.getVideoById(idMovie,
            Constantes.API_KEY, Constantes.LANGUAJE)

        try{
            var response = resp.execute()
            if(response.isSuccessful){
                var body = response.body()
                if(body !=null){
                    return body.results
                }else{
                    Log.e("respuesta ","body null")
                    return ArrayList<DetailVideo>()
                }
            }else{
                Log.e("respuesta ","no succesful")
                return ArrayList<DetailVideo>()
            }
        }catch(e:Exception){
            Log.e("respuesta catch",e.message.toString())
            return ArrayList<DetailVideo>()
        }
    }
}