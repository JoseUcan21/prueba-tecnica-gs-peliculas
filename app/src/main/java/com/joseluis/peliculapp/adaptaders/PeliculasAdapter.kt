package com.joseluis.peliculapp.adaptaders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.joseluis.peliculapp.R
import com.joseluis.peliculapp.data.models.DetailMovies
import com.joseluis.peliculapp.ui.fragments.MainFragmentDirections
import com.joseluis.peliculapp.utils.Constantes
import com.joseluis.peliculapp.utils.InternetConection


class PeliculasAdapter(var dataSet: ArrayList<DetailMovies>,val isPopular:Boolean) :
    RecyclerView.Adapter<PeliculasAdapter.ViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        val view = if(isPopular){
            LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.pelicula_item_popular, viewGroup, false)
        }
        else{
            LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.pelicula_item, viewGroup, false)
        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        viewHolder.enlazarItem(dataSet[position])
    }

    override fun getItemCount() = dataSet.size

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        private val imgPoster: ImageView
        private val contexto = view.context
        init {
            imgPoster = view.findViewById(R.id.imgPoster)
        }

        fun enlazarItem(objeto:DetailMovies){
            if(InternetConection.isOnline()){
                val urlImg = "${Constantes.URL_POSTER}${objeto.poster_path}"
                Glide.with(contexto).load(urlImg).into(imgPoster)
            }
            else{
                imgPoster.setImageResource(objeto.poster_id)
            }

            val action = MainFragmentDirections.actionMainFragmentToDetalleFragment(objeto)
            imgPoster.setOnClickListener {
                view.findNavController().navigate(action)
            }
        }
    }
}
