package com.joseluis.peliculapp.ui.fragments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.joseluis.peliculapp.adaptaders.PeliculasAdapter
import com.joseluis.peliculapp.databinding.MainFragmentBinding
import com.joseluis.peliculapp.ui.PeliculasActivity
import com.joseluis.peliculapp.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {

    lateinit var adaptador: PeliculasAdapter
    lateinit var adaptadorPopular: PeliculasAdapter

    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = MainFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as PeliculasActivity).title = "Películas en cines"
        viewModel.obtenerPlayingNowList()
        viewModel.obtenerPopularList()
        viewModel.listaPlayingNowMD.observe(viewLifecycleOwner,{
            adaptador = PeliculasAdapter(it,false)
            binding.recyclerMovies.adapter = adaptador
            binding.recyclerMovies.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        })

        viewModel.listaPopularMD.observe(viewLifecycleOwner,{
            adaptadorPopular = PeliculasAdapter(it,true)
            binding.recyclerPopular.adapter = adaptadorPopular
            binding.recyclerPopular.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        })

        viewModel.errorMsg.observe(viewLifecycleOwner,{
            (activity as PeliculasActivity).mostrarDialogo("Error $it")
            findNavController().popBackStack()
        })

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
    }

}