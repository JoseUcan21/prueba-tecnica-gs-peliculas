package com.joseluis.peliculapp.ui.fragments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.joseluis.peliculapp.R
import com.joseluis.peliculapp.adaptaders.PeliculasAdapter
import com.joseluis.peliculapp.databinding.DetalleFragmentBinding
import com.joseluis.peliculapp.databinding.MainFragmentBinding
import com.joseluis.peliculapp.ui.PeliculasActivity
import com.joseluis.peliculapp.utils.Constantes
import com.joseluis.peliculapp.utils.InternetConection
import com.joseluis.peliculapp.viewmodels.DetalleViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetalleFragment : Fragment() {

    private var _binding: DetalleFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: DetalleViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DetalleFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        viewModel = ViewModelProvider(this).get(DetalleViewModel::class.java)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        initUi()
    }

    private fun initUi() {

        val args: DetalleFragmentArgs by navArgs()
        val mDetailMovies   = args.movieObject
        if(InternetConection.isOnline()){
            Glide.with(requireActivity()).load("${Constantes.URL_POSTER}${mDetailMovies.backdrop_path}").into(binding.imgBack)
            Glide.with(requireActivity()).load("${Constantes.URL_POSTER}${mDetailMovies.poster_path}").into(binding.imgPoster)
        }
        else{
            binding.imgBack.setImageResource(mDetailMovies.portada_id)
            binding.imgPoster.setImageResource(mDetailMovies.poster_id)
        }

        binding.txtTitulo.text      = mDetailMovies.original_title
        binding.txtSinopsis.text    = mDetailMovies.overview
        binding.txtRating.text      = mDetailMovies.vote_average.toString()

        //El botón de reproducir solo funciona con internet
        if (InternetConection.isOnline()){
            binding.btnReproducir.visibility = View.VISIBLE
        }else{
            binding.btnReproducir.visibility = View.GONE
        }

        binding.btnReproducir.setOnClickListener {
            if(InternetConection.isOnline()){
                val action = DetalleFragmentDirections.actionDetalleFragmentToVideoFragment(mDetailMovies.id.toString())
                findNavController().navigate(action)
            }
            else{
                (activity as PeliculasActivity).mostrarDialogo(Constantes.INTERNET_REQUERIDO)
            }
        }
    }

}