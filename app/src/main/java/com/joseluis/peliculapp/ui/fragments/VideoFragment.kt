package com.joseluis.peliculapp.ui.fragments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.joseluis.peliculapp.R
import com.joseluis.peliculapp.databinding.DetalleFragmentBinding
import com.joseluis.peliculapp.databinding.VideoFragmentBinding
import com.joseluis.peliculapp.viewmodels.VideoViewModel
import android.webkit.WebSettings
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.joseluis.peliculapp.ui.PeliculasActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VideoFragment : Fragment() {

    private var _binding: VideoFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: VideoViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = VideoFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        viewModel = ViewModelProvider(this).get(VideoViewModel::class.java)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val webSettings: WebSettings = binding.myWebView.getSettings()
        webSettings.javaScriptEnabled = true
        binding.myWebView.webViewClient = WebViewClient()
        val args: VideoFragmentArgs by navArgs()
        viewModel.obtenerVideosByMovieId(args.idVideo)

        //si trae un video lo reproducimos
        viewModel.keyYt.observe(viewLifecycleOwner,{
            if(it.isNotEmpty()){
                binding.myWebView.loadUrl(it)
            }
        })

        //si hay error, mostramos un mensaje
        viewModel.errorMsg.observe(viewLifecycleOwner,{
            (activity as PeliculasActivity).mostrarDialogo("Error $it")
            findNavController().popBackStack()
        })

    }


}