package com.joseluis.peliculapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.joseluis.peliculapp.R
import com.joseluis.peliculapp.databinding.ActivityPeliculasBinding
import com.joseluis.peliculapp.ui.dialogos.OkDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PeliculasActivity : AppCompatActivity() {
    lateinit var binding:ActivityPeliculasBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPeliculasBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }

    fun mostrarDialogo( mensaje:String){
        val newFragment = OkDialog(mensaje)
        newFragment.show(supportFragmentManager, "MensajeDialog")
    }
}