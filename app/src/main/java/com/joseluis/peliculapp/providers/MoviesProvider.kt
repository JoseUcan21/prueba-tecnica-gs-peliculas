package com.joseluis.peliculapp.providers

import android.content.Context
import com.joseluis.peliculapp.data.dao.MoviesDao
import com.joseluis.peliculapp.data.models.DetailMovies
import com.joseluis.peliculapp.data.models.DetailVideo
import com.joseluis.peliculapp.data.network.MovieService
import com.joseluis.peliculapp.utils.Constantes
import com.joseluis.peliculapp.utils.InternetConection
import javax.inject.Inject

class MoviesProvider @Inject constructor(
    private val mMovieService: MovieService,
    private val mMovieDao:MoviesDao
) {

    suspend fun getMoviesPlayingNow():ArrayList<DetailMovies>{
        //si hay internet consumimos api, si no, traemos lista default
        return if(InternetConection.isOnline()){
            mMovieService.recuperaNowPlayingApi()
        } else{
            mMovieDao.recuperaNowPlayingDb()
        }
    }

    suspend fun getMoviesPopular():ArrayList<DetailMovies>{
        return if(InternetConection.isOnline()){
            mMovieService.recuperaPopularApi()
        }else{
            mMovieDao.recuperaPopularDb()
        }
    }

    //este servicio solo funciona con internet
    suspend fun getVideoByMovie(id:String):ArrayList<DetailVideo>{
        return mMovieService.recuperaVideos(id)
    }
}