package com.joseluis.peliculapp.utils

import android.content.Context

object Constantes {

    lateinit var contextoGlobal: Context
    val URL_BASE    = "https://api.themoviedb.org/3/"
    val API_KEY     = "4996541610d99f140ee83be6661dbb84"
    val LANGUAJE    = "es-ES"
    val PAGE        = 1
    val URL_POSTER  = "https://image.tmdb.org/t/p/w500"
    val URL_YT      = "https://www.youtube.com/watch?v="
    val ERROR_VIDEO = "No se encontraron videos"
    val ERROR_MOVIE = "No se encontraron peiculas"
    val INTERNET_REQUERIDO = "Internet Requerido para esta acción"
}