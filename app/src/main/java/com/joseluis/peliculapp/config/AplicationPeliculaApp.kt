package com.joseluis.peliculapp.config

import android.app.Application
import com.joseluis.peliculapp.data.network.MovieApiClient
import com.joseluis.peliculapp.utils.Constantes
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AplicationPeliculaApp:Application() {
    override fun onCreate() {
        super.onCreate()
        Constantes.contextoGlobal = applicationContext
    }
}