package com.joseluis.peliculapp.config

import com.joseluis.peliculapp.data.network.MovieApiClient
import com.joseluis.peliculapp.utils.Constantes
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ConfigRetrofit {

    @Singleton
    @Provides
    fun obtenerConfiguracionRetofit(): MovieApiClient {
        val mRetrofit = Retrofit.Builder()
            .baseUrl(Constantes.URL_BASE)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return mRetrofit.create(MovieApiClient::class.java)
    }
}