package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "com.joseluis.peliculapp.viewmodels.VideoViewModel_HiltModules.BindsModule"
)
public class _com_joseluis_peliculapp_viewmodels_VideoViewModel_HiltModules_BindsModule {
}
