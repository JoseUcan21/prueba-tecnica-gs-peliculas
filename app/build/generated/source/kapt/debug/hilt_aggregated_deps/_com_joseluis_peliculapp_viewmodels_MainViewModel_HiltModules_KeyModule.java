package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityRetainedComponent",
    modules = "com.joseluis.peliculapp.viewmodels.MainViewModel_HiltModules.KeyModule"
)
public class _com_joseluis_peliculapp_viewmodels_MainViewModel_HiltModules_KeyModule {
}
