package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityComponent",
    entryPoints = "com.joseluis.peliculapp.ui.PeliculasActivity_GeneratedInjector"
)
public class _com_joseluis_peliculapp_ui_PeliculasActivity_GeneratedInjector {
}
