package dagger.hilt.internal.processedrootsentinel.codegen;

import dagger.hilt.internal.processedrootsentinel.ProcessedRootSentinel;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@ProcessedRootSentinel(
    roots = "com.joseluis.peliculapp.config.AplicationPeliculaApp"
)
public class _com_joseluis_peliculapp_config_AplicationPeliculaApp {
}
