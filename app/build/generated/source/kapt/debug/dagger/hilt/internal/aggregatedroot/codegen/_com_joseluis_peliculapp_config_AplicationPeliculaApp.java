package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.HiltAndroidApp;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "com.joseluis.peliculapp.config.AplicationPeliculaApp",
    originatingRoot = "com.joseluis.peliculapp.config.AplicationPeliculaApp",
    rootAnnotation = HiltAndroidApp.class
)
public class _com_joseluis_peliculapp_config_AplicationPeliculaApp {
}
