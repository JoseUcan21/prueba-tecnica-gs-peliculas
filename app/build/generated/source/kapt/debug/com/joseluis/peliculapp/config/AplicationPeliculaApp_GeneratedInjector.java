package com.joseluis.peliculapp.config;

import dagger.hilt.InstallIn;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.components.SingletonComponent;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = AplicationPeliculaApp.class
)
@GeneratedEntryPoint
@InstallIn(SingletonComponent.class)
public interface AplicationPeliculaApp_GeneratedInjector {
  void injectAplicationPeliculaApp(AplicationPeliculaApp aplicationPeliculaApp);
}
