package com.joseluis.peliculapp.ui;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = PeliculasActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface PeliculasActivity_GeneratedInjector {
  void injectPeliculasActivity(PeliculasActivity peliculasActivity);
}
