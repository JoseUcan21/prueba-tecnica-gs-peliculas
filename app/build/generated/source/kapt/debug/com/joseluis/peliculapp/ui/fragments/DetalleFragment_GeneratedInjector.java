package com.joseluis.peliculapp.ui.fragments;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = DetalleFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface DetalleFragment_GeneratedInjector {
  void injectDetalleFragment(DetalleFragment detalleFragment);
}
