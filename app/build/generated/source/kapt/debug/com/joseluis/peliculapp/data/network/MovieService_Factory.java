// Generated by Dagger (https://dagger.dev).
package com.joseluis.peliculapp.data.network;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class MovieService_Factory implements Factory<MovieService> {
  private final Provider<MovieApiClient> apiClientProvider;

  public MovieService_Factory(Provider<MovieApiClient> apiClientProvider) {
    this.apiClientProvider = apiClientProvider;
  }

  @Override
  public MovieService get() {
    return newInstance(apiClientProvider.get());
  }

  public static MovieService_Factory create(Provider<MovieApiClient> apiClientProvider) {
    return new MovieService_Factory(apiClientProvider);
  }

  public static MovieService newInstance(MovieApiClient apiClient) {
    return new MovieService(apiClient);
  }
}
