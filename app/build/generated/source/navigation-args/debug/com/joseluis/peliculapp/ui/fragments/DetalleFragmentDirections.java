package com.joseluis.peliculapp.ui.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import com.joseluis.peliculapp.R;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class DetalleFragmentDirections {
  private DetalleFragmentDirections() {
  }

  @NonNull
  public static ActionDetalleFragmentToVideoFragment actionDetalleFragmentToVideoFragment(
      @NonNull String idVideo) {
    return new ActionDetalleFragmentToVideoFragment(idVideo);
  }

  public static class ActionDetalleFragmentToVideoFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    @SuppressWarnings("unchecked")
    private ActionDetalleFragmentToVideoFragment(@NonNull String idVideo) {
      if (idVideo == null) {
        throw new IllegalArgumentException("Argument \"idVideo\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("idVideo", idVideo);
    }

    @NonNull
    @SuppressWarnings("unchecked")
    public ActionDetalleFragmentToVideoFragment setIdVideo(@NonNull String idVideo) {
      if (idVideo == null) {
        throw new IllegalArgumentException("Argument \"idVideo\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("idVideo", idVideo);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("idVideo")) {
        String idVideo = (String) arguments.get("idVideo");
        __result.putString("idVideo", idVideo);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_detalleFragment_to_videoFragment;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getIdVideo() {
      return (String) arguments.get("idVideo");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionDetalleFragmentToVideoFragment that = (ActionDetalleFragmentToVideoFragment) object;
      if (arguments.containsKey("idVideo") != that.arguments.containsKey("idVideo")) {
        return false;
      }
      if (getIdVideo() != null ? !getIdVideo().equals(that.getIdVideo()) : that.getIdVideo() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getIdVideo() != null ? getIdVideo().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionDetalleFragmentToVideoFragment(actionId=" + getActionId() + "){"
          + "idVideo=" + getIdVideo()
          + "}";
    }
  }
}
