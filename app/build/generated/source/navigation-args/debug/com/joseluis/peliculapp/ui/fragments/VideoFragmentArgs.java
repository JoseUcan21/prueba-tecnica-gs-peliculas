package com.joseluis.peliculapp.ui.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class VideoFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private VideoFragmentArgs() {
  }

  @SuppressWarnings("unchecked")
  private VideoFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static VideoFragmentArgs fromBundle(@NonNull Bundle bundle) {
    VideoFragmentArgs __result = new VideoFragmentArgs();
    bundle.setClassLoader(VideoFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("idVideo")) {
      String idVideo;
      idVideo = bundle.getString("idVideo");
      if (idVideo == null) {
        throw new IllegalArgumentException("Argument \"idVideo\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("idVideo", idVideo);
    } else {
      throw new IllegalArgumentException("Required argument \"idVideo\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public String getIdVideo() {
    return (String) arguments.get("idVideo");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("idVideo")) {
      String idVideo = (String) arguments.get("idVideo");
      __result.putString("idVideo", idVideo);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    VideoFragmentArgs that = (VideoFragmentArgs) object;
    if (arguments.containsKey("idVideo") != that.arguments.containsKey("idVideo")) {
      return false;
    }
    if (getIdVideo() != null ? !getIdVideo().equals(that.getIdVideo()) : that.getIdVideo() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getIdVideo() != null ? getIdVideo().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "VideoFragmentArgs{"
        + "idVideo=" + getIdVideo()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    @SuppressWarnings("unchecked")
    public Builder(VideoFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    @SuppressWarnings("unchecked")
    public Builder(@NonNull String idVideo) {
      if (idVideo == null) {
        throw new IllegalArgumentException("Argument \"idVideo\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("idVideo", idVideo);
    }

    @NonNull
    public VideoFragmentArgs build() {
      VideoFragmentArgs result = new VideoFragmentArgs(arguments);
      return result;
    }

    @NonNull
    @SuppressWarnings("unchecked")
    public Builder setIdVideo(@NonNull String idVideo) {
      if (idVideo == null) {
        throw new IllegalArgumentException("Argument \"idVideo\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("idVideo", idVideo);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getIdVideo() {
      return (String) arguments.get("idVideo");
    }
  }
}
