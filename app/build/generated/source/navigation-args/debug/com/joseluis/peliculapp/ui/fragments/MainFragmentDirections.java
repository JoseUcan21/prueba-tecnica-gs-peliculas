package com.joseluis.peliculapp.ui.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import com.joseluis.peliculapp.R;
import com.joseluis.peliculapp.data.models.DetailMovies;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class MainFragmentDirections {
  private MainFragmentDirections() {
  }

  @NonNull
  public static ActionMainFragmentToDetalleFragment actionMainFragmentToDetalleFragment(
      @NonNull DetailMovies movieObject) {
    return new ActionMainFragmentToDetalleFragment(movieObject);
  }

  public static class ActionMainFragmentToDetalleFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    @SuppressWarnings("unchecked")
    private ActionMainFragmentToDetalleFragment(@NonNull DetailMovies movieObject) {
      if (movieObject == null) {
        throw new IllegalArgumentException("Argument \"movieObject\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("movieObject", movieObject);
    }

    @NonNull
    @SuppressWarnings("unchecked")
    public ActionMainFragmentToDetalleFragment setMovieObject(@NonNull DetailMovies movieObject) {
      if (movieObject == null) {
        throw new IllegalArgumentException("Argument \"movieObject\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("movieObject", movieObject);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("movieObject")) {
        DetailMovies movieObject = (DetailMovies) arguments.get("movieObject");
        if (Parcelable.class.isAssignableFrom(DetailMovies.class) || movieObject == null) {
          __result.putParcelable("movieObject", Parcelable.class.cast(movieObject));
        } else if (Serializable.class.isAssignableFrom(DetailMovies.class)) {
          __result.putSerializable("movieObject", Serializable.class.cast(movieObject));
        } else {
          throw new UnsupportedOperationException(DetailMovies.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_mainFragment_to_detalleFragment;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public DetailMovies getMovieObject() {
      return (DetailMovies) arguments.get("movieObject");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionMainFragmentToDetalleFragment that = (ActionMainFragmentToDetalleFragment) object;
      if (arguments.containsKey("movieObject") != that.arguments.containsKey("movieObject")) {
        return false;
      }
      if (getMovieObject() != null ? !getMovieObject().equals(that.getMovieObject()) : that.getMovieObject() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getMovieObject() != null ? getMovieObject().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionMainFragmentToDetalleFragment(actionId=" + getActionId() + "){"
          + "movieObject=" + getMovieObject()
          + "}";
    }
  }
}
