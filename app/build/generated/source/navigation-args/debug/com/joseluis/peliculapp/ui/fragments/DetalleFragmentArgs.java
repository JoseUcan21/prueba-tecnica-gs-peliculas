package com.joseluis.peliculapp.ui.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import com.joseluis.peliculapp.data.models.DetailMovies;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class DetalleFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private DetalleFragmentArgs() {
  }

  @SuppressWarnings("unchecked")
  private DetalleFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static DetalleFragmentArgs fromBundle(@NonNull Bundle bundle) {
    DetalleFragmentArgs __result = new DetalleFragmentArgs();
    bundle.setClassLoader(DetalleFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("movieObject")) {
      DetailMovies movieObject;
      if (Parcelable.class.isAssignableFrom(DetailMovies.class) || Serializable.class.isAssignableFrom(DetailMovies.class)) {
        movieObject = (DetailMovies) bundle.get("movieObject");
      } else {
        throw new UnsupportedOperationException(DetailMovies.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (movieObject == null) {
        throw new IllegalArgumentException("Argument \"movieObject\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("movieObject", movieObject);
    } else {
      throw new IllegalArgumentException("Required argument \"movieObject\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public DetailMovies getMovieObject() {
    return (DetailMovies) arguments.get("movieObject");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("movieObject")) {
      DetailMovies movieObject = (DetailMovies) arguments.get("movieObject");
      if (Parcelable.class.isAssignableFrom(DetailMovies.class) || movieObject == null) {
        __result.putParcelable("movieObject", Parcelable.class.cast(movieObject));
      } else if (Serializable.class.isAssignableFrom(DetailMovies.class)) {
        __result.putSerializable("movieObject", Serializable.class.cast(movieObject));
      } else {
        throw new UnsupportedOperationException(DetailMovies.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    DetalleFragmentArgs that = (DetalleFragmentArgs) object;
    if (arguments.containsKey("movieObject") != that.arguments.containsKey("movieObject")) {
      return false;
    }
    if (getMovieObject() != null ? !getMovieObject().equals(that.getMovieObject()) : that.getMovieObject() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getMovieObject() != null ? getMovieObject().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "DetalleFragmentArgs{"
        + "movieObject=" + getMovieObject()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    @SuppressWarnings("unchecked")
    public Builder(DetalleFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    @SuppressWarnings("unchecked")
    public Builder(@NonNull DetailMovies movieObject) {
      if (movieObject == null) {
        throw new IllegalArgumentException("Argument \"movieObject\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("movieObject", movieObject);
    }

    @NonNull
    public DetalleFragmentArgs build() {
      DetalleFragmentArgs result = new DetalleFragmentArgs(arguments);
      return result;
    }

    @NonNull
    @SuppressWarnings("unchecked")
    public Builder setMovieObject(@NonNull DetailMovies movieObject) {
      if (movieObject == null) {
        throw new IllegalArgumentException("Argument \"movieObject\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("movieObject", movieObject);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public DetailMovies getMovieObject() {
      return (DetailMovies) arguments.get("movieObject");
    }
  }
}
