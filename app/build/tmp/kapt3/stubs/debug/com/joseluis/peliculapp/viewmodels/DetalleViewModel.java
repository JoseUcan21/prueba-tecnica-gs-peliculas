package com.joseluis.peliculapp.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/joseluis/peliculapp/viewmodels/DetalleViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "app_debug"})
public final class DetalleViewModel extends androidx.lifecycle.ViewModel {
    
    public DetalleViewModel() {
        super();
    }
}