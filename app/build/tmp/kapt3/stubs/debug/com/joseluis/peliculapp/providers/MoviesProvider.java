package com.joseluis.peliculapp.providers;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J!\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\nH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ!\u0010\f\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\nH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ)\u0010\r\u001a\u0012\u0012\u0004\u0012\u00020\u000e0\bj\b\u0012\u0004\u0012\u00020\u000e`\n2\u0006\u0010\u000f\u001a\u00020\u0010H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0012"}, d2 = {"Lcom/joseluis/peliculapp/providers/MoviesProvider;", "", "mMovieService", "Lcom/joseluis/peliculapp/data/network/MovieService;", "mMovieDao", "Lcom/joseluis/peliculapp/data/dao/MoviesDao;", "(Lcom/joseluis/peliculapp/data/network/MovieService;Lcom/joseluis/peliculapp/data/dao/MoviesDao;)V", "getMoviesPlayingNow", "Ljava/util/ArrayList;", "Lcom/joseluis/peliculapp/data/models/DetailMovies;", "Lkotlin/collections/ArrayList;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getMoviesPopular", "getVideoByMovie", "Lcom/joseluis/peliculapp/data/models/DetailVideo;", "id", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class MoviesProvider {
    private final com.joseluis.peliculapp.data.network.MovieService mMovieService = null;
    private final com.joseluis.peliculapp.data.dao.MoviesDao mMovieDao = null;
    
    @javax.inject.Inject()
    public MoviesProvider(@org.jetbrains.annotations.NotNull()
    com.joseluis.peliculapp.data.network.MovieService mMovieService, @org.jetbrains.annotations.NotNull()
    com.joseluis.peliculapp.data.dao.MoviesDao mMovieDao) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getMoviesPlayingNow(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getMoviesPopular(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getVideoByMovie(@org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailVideo>> continuation) {
        return null;
    }
}