package com.joseluis.peliculapp.data.dao;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J!\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ\u0016\u0010\t\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006R\u001e\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\n"}, d2 = {"Lcom/joseluis/peliculapp/data/dao/MoviesList;", "", "()V", "listaMovies", "Ljava/util/ArrayList;", "Lcom/joseluis/peliculapp/data/models/DetailMovies;", "Lkotlin/collections/ArrayList;", "getMoviesDefaultList", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getMoviesDefaultList2", "app_debug"})
public final class MoviesList {
    private java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies> listaMovies;
    
    public MoviesList() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getMoviesDefaultList(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies> getMoviesDefaultList2() {
        return null;
    }
}