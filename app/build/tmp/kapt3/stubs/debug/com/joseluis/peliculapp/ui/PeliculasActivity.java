package com.joseluis.peliculapp.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0012\u0010\r\u001a\u00020\n2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0014R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0010"}, d2 = {"Lcom/joseluis/peliculapp/ui/PeliculasActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "binding", "Lcom/joseluis/peliculapp/databinding/ActivityPeliculasBinding;", "getBinding", "()Lcom/joseluis/peliculapp/databinding/ActivityPeliculasBinding;", "setBinding", "(Lcom/joseluis/peliculapp/databinding/ActivityPeliculasBinding;)V", "mostrarDialogo", "", "mensaje", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class PeliculasActivity extends androidx.appcompat.app.AppCompatActivity {
    public com.joseluis.peliculapp.databinding.ActivityPeliculasBinding binding;
    
    public PeliculasActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.joseluis.peliculapp.databinding.ActivityPeliculasBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.joseluis.peliculapp.databinding.ActivityPeliculasBinding p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void mostrarDialogo(@org.jetbrains.annotations.NotNull()
    java.lang.String mensaje) {
    }
}