package com.joseluis.peliculapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0014\u0010\u000b\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006R\u0014\u0010\u000f\u001a\u00020\u0010X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0006R\u0014\u0010\u0015\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0006R\u0014\u0010\u0017\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0006R\u001a\u0010\u0019\u001a\u00020\u001aX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001e\u00a8\u0006\u001f"}, d2 = {"Lcom/joseluis/peliculapp/utils/Constantes;", "", "()V", "API_KEY", "", "getAPI_KEY", "()Ljava/lang/String;", "ERROR_MOVIE", "getERROR_MOVIE", "ERROR_VIDEO", "getERROR_VIDEO", "INTERNET_REQUERIDO", "getINTERNET_REQUERIDO", "LANGUAJE", "getLANGUAJE", "PAGE", "", "getPAGE", "()I", "URL_BASE", "getURL_BASE", "URL_POSTER", "getURL_POSTER", "URL_YT", "getURL_YT", "contextoGlobal", "Landroid/content/Context;", "getContextoGlobal", "()Landroid/content/Context;", "setContextoGlobal", "(Landroid/content/Context;)V", "app_debug"})
public final class Constantes {
    @org.jetbrains.annotations.NotNull()
    public static final com.joseluis.peliculapp.utils.Constantes INSTANCE = null;
    public static android.content.Context contextoGlobal;
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String URL_BASE = "https://api.themoviedb.org/3/";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String API_KEY = "4996541610d99f140ee83be6661dbb84";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String LANGUAJE = "es-ES";
    private static final int PAGE = 1;
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String URL_POSTER = "https://image.tmdb.org/t/p/w500";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String URL_YT = "https://www.youtube.com/watch?v=";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String ERROR_VIDEO = "No se encontraron videos";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String ERROR_MOVIE = "No se encontraron peiculas";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String INTERNET_REQUERIDO = "Internet Requerido para esta acci\u00f3n";
    
    private Constantes() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContextoGlobal() {
        return null;
    }
    
    public final void setContextoGlobal(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getURL_BASE() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAPI_KEY() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLANGUAJE() {
        return null;
    }
    
    public final int getPAGE() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getURL_POSTER() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getURL_YT() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getERROR_VIDEO() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getERROR_MOVIE() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getINTERNET_REQUERIDO() {
        return null;
    }
}