package com.joseluis.peliculapp.viewmodels;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0014\u001a\u00020\u0015J\u0006\u0010\u0016\u001a\u00020\u0015R \u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR&\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\t\"\u0004\b\u0010\u0010\u000bR&\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\t\"\u0004\b\u0013\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/joseluis/peliculapp/viewmodels/MainViewModel;", "Landroidx/lifecycle/ViewModel;", "moviesProvider", "Lcom/joseluis/peliculapp/providers/MoviesProvider;", "(Lcom/joseluis/peliculapp/providers/MoviesProvider;)V", "errorMsg", "Landroidx/lifecycle/MutableLiveData;", "", "getErrorMsg", "()Landroidx/lifecycle/MutableLiveData;", "setErrorMsg", "(Landroidx/lifecycle/MutableLiveData;)V", "listaPlayingNowMD", "Ljava/util/ArrayList;", "Lcom/joseluis/peliculapp/data/models/DetailMovies;", "getListaPlayingNowMD", "setListaPlayingNowMD", "listaPopularMD", "getListaPopularMD", "setListaPopularMD", "obtenerPlayingNowList", "", "obtenerPopularList", "app_debug"})
public final class MainViewModel extends androidx.lifecycle.ViewModel {
    private final com.joseluis.peliculapp.providers.MoviesProvider moviesProvider = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> listaPlayingNowMD;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> listaPopularMD;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> errorMsg;
    
    @javax.inject.Inject()
    public MainViewModel(@org.jetbrains.annotations.NotNull()
    com.joseluis.peliculapp.providers.MoviesProvider moviesProvider) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> getListaPlayingNowMD() {
        return null;
    }
    
    public final void setListaPlayingNowMD(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> getListaPopularMD() {
        return null;
    }
    
    public final void setListaPopularMD(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getErrorMsg() {
        return null;
    }
    
    public final void setErrorMsg(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    public final void obtenerPlayingNowList() {
    }
    
    public final void obtenerPopularList() {
    }
}