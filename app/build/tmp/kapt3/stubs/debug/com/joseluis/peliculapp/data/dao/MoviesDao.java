package com.joseluis.peliculapp.data.dao;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0002J!\u0010\t\u001a\u0012\u0012\u0004\u0012\u00020\u000b0\nj\b\u0012\u0004\u0012\u00020\u000b`\fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ!\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u000b0\nj\b\u0012\u0004\u0012\u00020\u000b`\fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rR\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000f"}, d2 = {"Lcom/joseluis/peliculapp/data/dao/MoviesDao;", "", "()V", "mMoviesList", "Lcom/joseluis/peliculapp/data/dao/MoviesList;", "getMMoviesList", "()Lcom/joseluis/peliculapp/data/dao/MoviesList;", "setMMoviesList", "(Lcom/joseluis/peliculapp/data/dao/MoviesList;)V", "recuperaNowPlayingDb", "Ljava/util/ArrayList;", "Lcom/joseluis/peliculapp/data/models/DetailMovies;", "Lkotlin/collections/ArrayList;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "recuperaPopularDb", "app_debug"})
public final class MoviesDao {
    public com.joseluis.peliculapp.data.dao.MoviesList mMoviesList;
    
    @javax.inject.Inject()
    public MoviesDao() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.joseluis.peliculapp.data.dao.MoviesList getMMoviesList() {
        return null;
    }
    
    public final void setMMoviesList(@org.jetbrains.annotations.NotNull()
    com.joseluis.peliculapp.data.dao.MoviesList p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object recuperaNowPlayingDb(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object recuperaPopularDb(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> continuation) {
        return null;
    }
}