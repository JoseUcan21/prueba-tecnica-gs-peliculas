package com.joseluis.peliculapp.ui.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J&\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\u001a\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u001e\u001a\u00020\u00182\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0006X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\b\"\u0004\b\r\u0010\nR\u0014\u0010\u000e\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcom/joseluis/peliculapp/ui/fragments/MainFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/joseluis/peliculapp/databinding/MainFragmentBinding;", "adaptador", "Lcom/joseluis/peliculapp/adaptaders/PeliculasAdapter;", "getAdaptador", "()Lcom/joseluis/peliculapp/adaptaders/PeliculasAdapter;", "setAdaptador", "(Lcom/joseluis/peliculapp/adaptaders/PeliculasAdapter;)V", "adaptadorPopular", "getAdaptadorPopular", "setAdaptadorPopular", "binding", "getBinding", "()Lcom/joseluis/peliculapp/databinding/MainFragmentBinding;", "viewModel", "Lcom/joseluis/peliculapp/viewmodels/MainViewModel;", "onActivityCreated", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onViewCreated", "view", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class MainFragment extends androidx.fragment.app.Fragment {
    public com.joseluis.peliculapp.adaptaders.PeliculasAdapter adaptador;
    public com.joseluis.peliculapp.adaptaders.PeliculasAdapter adaptadorPopular;
    private com.joseluis.peliculapp.databinding.MainFragmentBinding _binding;
    private com.joseluis.peliculapp.viewmodels.MainViewModel viewModel;
    
    public MainFragment() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.joseluis.peliculapp.adaptaders.PeliculasAdapter getAdaptador() {
        return null;
    }
    
    public final void setAdaptador(@org.jetbrains.annotations.NotNull()
    com.joseluis.peliculapp.adaptaders.PeliculasAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.joseluis.peliculapp.adaptaders.PeliculasAdapter getAdaptadorPopular() {
        return null;
    }
    
    public final void setAdaptadorPopular(@org.jetbrains.annotations.NotNull()
    com.joseluis.peliculapp.adaptaders.PeliculasAdapter p0) {
    }
    
    private final com.joseluis.peliculapp.databinding.MainFragmentBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onActivityCreated(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
}