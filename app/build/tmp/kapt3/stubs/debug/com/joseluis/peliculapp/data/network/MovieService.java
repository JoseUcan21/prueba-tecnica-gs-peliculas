package com.joseluis.peliculapp.data.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J!\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ!\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ)\u0010\u000b\u001a\u0012\u0012\u0004\u0012\u00020\f0\u0006j\b\u0012\u0004\u0012\u00020\f`\b2\u0006\u0010\r\u001a\u00020\u000eH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0010"}, d2 = {"Lcom/joseluis/peliculapp/data/network/MovieService;", "", "apiClient", "Lcom/joseluis/peliculapp/data/network/MovieApiClient;", "(Lcom/joseluis/peliculapp/data/network/MovieApiClient;)V", "recuperaNowPlayingApi", "Ljava/util/ArrayList;", "Lcom/joseluis/peliculapp/data/models/DetailMovies;", "Lkotlin/collections/ArrayList;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "recuperaPopularApi", "recuperaVideos", "Lcom/joseluis/peliculapp/data/models/DetailVideo;", "idMovie", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class MovieService {
    private final com.joseluis.peliculapp.data.network.MovieApiClient apiClient = null;
    
    @javax.inject.Inject()
    public MovieService(@org.jetbrains.annotations.NotNull()
    com.joseluis.peliculapp.data.network.MovieApiClient apiClient) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object recuperaNowPlayingApi(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object recuperaPopularApi(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailMovies>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object recuperaVideos(@org.jetbrains.annotations.NotNull()
    java.lang.String idMovie, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.ArrayList<com.joseluis.peliculapp.data.models.DetailVideo>> continuation) {
        return null;
    }
}