# Prueba Tecnica Gs Peliculas

Ejercicio Técnico (App de Películas/Series)

## Librerías Utilizadas
- Retrofit
- Glide
- Dagger Hilt

## Detalles no implementados pero abiertos a mejora
- Implementar un Broadcast Receiver para detectar el cambio de Red a través del Network Manager. Esto en
caso de que reproduzcan un video o se caiga el internet en el proceso

- La app offline muestra por default 5 películas. Estas películas están harcodeadas en un Object para no
crear una instancia y usarlas. Esto puede mejorarse usando una base de datos en Room o Realm en vez de un archivo
Offline no reproduce video pero si muestra info de la película

- La api devuelve un key para ver un video en YT... El video se muestra en un webview. Una mejora sería descargar
el stream del video("tipo netflix") y puedo reproducirse en OffLine